<?php
/**
 * Template Name: Detail produktu
 */
?>


<?php get_template_part('templates/page', 'header'); ?>
<div class="page_detail_produktu_holder section_pad_70">
	<div class="container">
		<div class="row">
			<div class="col-3">
				<ul class="katalog_nav">
					<li><a href="#">Potrubí</a></li>
					<li><a href="#">Systémové desky</a></li>
					<li><a href="#">Regulace</a></li>
					<li><a href="#">Rozdělovače, skříně</a></li>
					<li><a href="#">Komponenty pro kompletaci</a></li>
				</ul>
			</div>
			<div class="col-9 detail_produkt_items_holder">
				<div id="potrubi" role="tablist" aria-multiselectable="true">
					<div class="card">
						<div class="card-header" role="tab" id="headingOne">
							<div class="produkt_img" style="background: url(<?= get_template_directory_uri() . '/dist/images/produkt.png' ?>) center / contain  no-repeat;"></div>
							<h5 class="mb-0">
								<a data-toggle="collapse" data-parent="#potrubi" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									TOP 226P, 232P, 240P, 250P - TOPTHERM AL - PERT, vícevrstvé trubky s hliníkovou vložkou pro podlahové a radiátorové vytápění
								</a>
							</h5>
							<a class="produkt_btn" data-toggle="collapse" data-parent="#potrubi" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"></a>
						</div>
						<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="card-block">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
								<table>
									<thead>
										<tr>
											<th>Název</th>
											<th>Typ</th>
											<th>Typ</th>
											<th>Balení</th>
											<th>Kč/m</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td rowspan="3">TOPTHERM PE-X,<br>
												trubka PE-Xc s kyslíkovou bariérou</td>
											<td>OP 116</td>
											<td>16 x 2</td>
											<td>200 mm</td>
											<td>23,-</td>
										</tr>
										<tr>
											<td>TOP 118</td>
											<td>18 x 2</td>
											<td>200 mm</td>
											<td>28,-</td>
										</tr>
										<tr>
											<td>TOP 120</td>
											<td>20 x 2</td>
											<td>200 mm</td>
											<td>30,-</td>
										</tr>
									</tbody>
								</table>
								<p>
									<a href="#"><u>Základní technická data trubek TOPTHERM PE-Xc</u></a>
								</p>
								<p>
									<a href="#">
										<img src="<?= get_template_directory_uri() . '/dist/images/produkt.png' ?>" alt="">
									</a>
									<a href="#">
										<img src="<?= get_template_directory_uri() . '/dist/images/produkt2.png' ?>" alt="">
									</a>
								</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" role="tab" id="headingTwo">
							<div class="produkt_img" style="background: url(<?= get_template_directory_uri() . '/dist/images/produkt2.png' ?>) center / contain  no-repeat;"></div>
							<h5 class="mb-0">
								<a class="collapsed" data-toggle="collapse" data-parent="#potrubi" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									Collapsible Group Item #2
								</a>
							</h5>
							<a class="produkt_btn" data-toggle="collapse" data-parent="#potrubi" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"></a>
						</div>
						<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="card-block">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header" role="tab" id="headingThree">
							<div class="produkt_img" style=""></div>
							<h5 class="mb-0">
								<a class="collapsed" data-toggle="collapse" data-parent="#potrubi" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									Collapsible Group Item #3
								</a>
							</h5>
							<a class="produkt_btn" data-toggle="collapse" data-parent="#potrubi" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"></a>
						</div>
						<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="card-block">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
