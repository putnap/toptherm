<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes)
{
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (Setup\display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more()
{
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


function custom_breadcrumbs()
{

    // Settings

    $breadcrums_id = 'breadcrumbs';
    $breadcrums_class = 'breadcrumb';
    $home_title = 'Úvod';

    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy = 'product_cat';

    // Get the query & post information
    global $post, $wp_query;

    // Do not display on the homepage
    if (!is_front_page()) {

        // Build the breadcrums
        echo '<ol id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';

        // Home page
        echo '<li class="breadcrumb-item item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';

        if (is_archive() && !is_tax() && !is_category() && !is_tag()) {

            echo '<li class="breadcrumb-item active item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';

        } else if (is_archive() && is_tax() && !is_category() && !is_tag()) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if ($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="breadcrumb-item item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';

            }

            $custom_tax_name = get_queried_object()->name;
            echo '<li class="breadcrumb-item active item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';

        } else if (is_single()) {

            // If post is a custom post type
            $post_type = get_post_type();

            // If it is a custom post type display name and link
            if ($post_type != 'post') {

                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);

                echo '<li class="breadcrumb-item item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';

            }

            // Get post category info
            $category = get_the_category();

            if (!empty($category)) {

                // Get last category post is in
                $last_category = end(array_values($category));

                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','), ',');
                $cat_parents = explode(',', $get_cat_parents);

                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach ($cat_parents as $parents) {
                    $cat_display .= '<li class="breadcrumb-item item-cat">' . $parents . '</li>';
                }

            }

            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if (empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {

                $taxonomy_terms = get_the_terms($post->ID, $custom_taxonomy);
                $cat_id = $taxonomy_terms[0]->term_id;
                $cat_nicename = $taxonomy_terms[0]->slug;
                $cat_link = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name = $taxonomy_terms[0]->name;

            }

            // Check if the post is in a category
            if (!empty($last_category)) {
                echo $cat_display;
                echo '<li class="breadcrumb-item active item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

                // Else if post is in a custom taxonomy
            } else if (!empty($cat_id)) {

                echo '<li class="breadcrumb-item item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="breadcrumb-item active item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            } else {

                echo '<li class="breadcrumb-item active item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';

            }

        } else if (is_category()) {

            // Category page
            echo '<li class="breadcrumb-item active item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';

        } else if (is_page()) {

            // Standard page
            if ($post->post_parent) {

                // If child page, get parents
                $anc = get_post_ancestors($post->ID);

                // Get parents in the right order
                $anc = array_reverse($anc);

                // Parent page loop
                if (!isset($parents)) $parents = null;
                foreach ($anc as $ancestor) {
                    $parents .= '<li class="breadcrumb-item item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                }

                // Display parent pages
                echo $parents;

                // Current page
                echo '<li class="breadcrumb-item active item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';

            } else {

                // Just display current page if not parents
                echo '<li class="breadcrumb-item active item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';

            }

        }

        echo '</ol>';

    }

}

function dejReference()
{
    ob_start();
    $args2 = array(
        'post_type' => 'reference',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => -1,
    );
    $reference = new \WP_Query($args2);
    ?>
    <div class="reference_slider_holder section_pad_70">
        <div class="reference_slider">
            <div class="owl-carousel">
                <?php
                while ($reference->have_posts()) : $reference->the_post(); ?>
                    <div class="reference_slider_item">
                        <div class="reference_slider_foto"
                             style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'medium') ?>) center no-repeat / cover;"></div>
                        <a href="<?= get_permalink() ?>" class="reference_slider_overlay">
                            <span class="reference_slider_name"><?= get_the_title() ?></span>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="owl_custom_navigation">
                <div class="reference_prev"></div>
                <div class="reference_next"></div>
            </div>
        </div>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('dejReference', __NAMESPACE__ . '\\dejReference');


function dejAkce()
{
    ob_start();
    $args3 = array(
        'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => 2,
    );
    $akce = new \WP_Query($args3);
    ?>
    <div class="akce_holder section_pad_70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="heading_with_line">Akce</h2>
                    <a href="<?= get_post_type_archive_link('post') ?>" class="akce_btn_all">ZOBRAZIT VŠE</a>
                </div>
            </div>
            <div class="row akce_item_holder">
                <?php
                while ($akce->have_posts()) : $akce->the_post(); ?>
                    <div class="col-md-6 col-12">
                        <div class="akce_item row">
							<div class="col-xl-4 col-lg-4 col-md-12 col-sm-4 col-5">
								<a class="akce_bg" href="<?= get_permalink(); ?>" style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'rp_pdf'); ?>) left top no-repeat;"></a>
							</div>
							<div class="col-xl-8 col-lg-8 col-md-12 col-sm-8 col-7">
								<h4><?= get_the_title() ?></h4>
								<div class="akce_date"><?= get_the_date() ?></div>
								<p><?= wp_trim_words(get_the_content(), 55, '...') ?></p>
								<a href="<?= get_permalink(); ?>" class="akce_item_btn">ČÍST CELÉ</a>
							</div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('dejAkce', __NAMESPACE__ . '\\dejAkce');


function dejProdukty()
{
    ob_start();
    $terms = get_terms(array(
        'taxonomy' => 'produkt_cat',
        'hide_empty' => false,
    ));
    ?>
    <div class="home_produkty_holder section_pad_70">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="heading_with_line">Produkty</h2>
                </div>
            </div>
            <div class="row home_produkty_items_holder">
                <div class="col-md-6 col-12">
                    <ul>
                        <?php
                        foreach ($terms as $term) { ?>
                            <li data-img="<?= get_field('nahledovy_obrazek', $term) ?>"><a
                                        href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-md-6 hidden-sm-down home_produkty_items_foto text-center">
					<div style="background: url(<?= get_template_directory_uri() . '/dist/images/produkty.png' ?>) center no-repeat;"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
    $content = ob_get_clean();
    return $content;
}

add_shortcode('dejProdukty', __NAMESPACE__ . '\\dejProdukty');

function filter_category_title($title) {
	$title = str_replace('Kategorie: ', '', $title);
	$title = str_replace('Archiv: ', '', $title);
	return $title;
}
add_filter('get_the_archive_title', __NAMESPACE__ . '\\filter_category_title');