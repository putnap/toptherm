<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
 * Theme setup
 */
function setup()
{
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-js-to-footer');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    add_image_size('produkt_thumb', 111, 119);
    add_image_size('rp_pdf', 130, 200);

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'top_navigation' => __('Top Navigation', 'sage'),
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    add_editor_style(Assets\asset_path('styles/main.css'));
}

add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

function new_submenu_class($menu)
{
    $menu = preg_replace('/ class="sub-menu"/', '/ class="dropdown-menu" /', $menu);
    return $menu;
}

add_filter('wp_nav_menu', __NAMESPACE__ . '\\new_submenu_class');

show_admin_bar(false);

/**
 * Register sidebars
 */
function widgets_init()
{
    register_sidebar([
        'name' => __('Footer 1', 'sage'),
        'id' => 'sidebar-footer1',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
    register_sidebar([
        'name' => __('Footer 2', 'sage'),
        'id' => 'sidebar-footer2',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
    register_sidebar([
        'name' => __('Footer 3', 'sage'),
        'id' => 'sidebar-footer3',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
    register_sidebar([
        'name' => __('Footer 4', 'sage'),
        'id' => 'sidebar-footer4',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ]);
}

add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar()
{
    static $display;

    isset($display) || $display = !in_array(true, [
        // The sidebar will NOT be displayed if ANY of the following return true.
        // @link https://codex.wordpress.org/Conditional_Tags
        is_404(),
        is_front_page(),
        is_page_template('template-custom.php'),
    ]);

    return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets()
{
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, null);
    wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
}

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


add_action('init', __NAMESPACE__ . '\\posty');
function posty()
{
    register_post_type('reference',
        array(
            'labels' => array(
                'name' => __('Reference'),
                'singular_name' => __('Reference')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'reference'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('slider',
        array(
            'labels' => array(
                'name' => __('Slider'),
                'singular_name' => __('Slider')
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'slider'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
    register_post_type('produkt',
        array(
            'labels' => array(
                'name' => __('Produkty'),
                'singular_name' => __('Produkt')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'produkty'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'
            ),
        )
    );
}

add_action('init', __NAMESPACE__ . '\\taxonomies', 0);
function taxonomies()
{
    register_taxonomy(
        'produkt_cat',
        'produkt',
        array(
            'labels' => array(
                'name' => 'Kategorie'
            ),
            'show_ui' => true,
            'show_admin_column' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}