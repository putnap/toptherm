<?php
/**
 * Template Name: Katalog
 */

$terms = get_terms(array(
    'taxonomy' => 'produkt_cat',
    'hide_empty' => true,
));


get_template_part('templates/page', 'header'); ?>
<div class="page_katalog_holder section_pad_70">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-12">
                <ul class="katalog_nav">
                    <?php
                    $act = get_queried_object()->term_id;
                    foreach ($terms as $term) {
                    	if( $term->term_id == $act){
                    		$popis = get_field('popis_katalog',$term);
						} ?>
                        <li <?= $term->term_id == $act ? 'class="active"' : '' ?>><a
                                    href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-lg-9 col-md-8 col-12 detail_produkt_items_holder">
				<?php if($popis != ''){ ?>
					<div class="cat_popis"><?= $popis; ?></div>
				<?php } ?>
                <div id="potrubi" role="tablist" aria-multiselectable="true">
                    <?php
                    $index = 0;
                    while (have_posts()) : the_post(); ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="heading<?= $index ?>">
                                <div class="produkt_img"
                                     style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'thumbnail') ?>) center / contain  no-repeat;"></div>
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#potrubi"
                                       href="#collapse<?= $index ?>" aria-expanded="false"
                                       aria-controls="collapse<?= $index ?>">
                                        <?= get_the_title() ?>
                                    </a>
                                </h5>
                                <a class="produkt_btn" data-toggle="collapse" data-parent="#potrubi"
                                   href="#collapse<?= $index ?>" aria-expanded="false"
                                   aria-controls="collapse<?= $index ?>"></a>
                            </div>
                            <div id="collapse<?= $index ?>" class="collapse" role="tabpanel"
                                 aria-labelledby="heading<?= $index ?>">
                                <div class="card-block">
                                    <?php the_content(); ?>
									<?php
                                    $gal = get_field('galerie');

                                    if (count($gal) && $gal != '' ) { ?>
                                        <div class="gal_hlder">
                                            <?php foreach ($gal as $img) { ?>
												<a href="<?= $img['url'] ?>">
													<img src="<?= $img['sizes']['produkt_thumb'] ?>" alt="">
												</a>
                                            <?php }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                        $index++;
                    endwhile; ?>
                </div>
            </div>

        </div>
    </div>
</div>
