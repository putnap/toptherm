<?php get_template_part('templates/page', 'header'); ?>
<div class="archiv akce_holder">
    <div class="container">
        <div class="row akce_item_holder">
            <?php while (have_posts()) :
                the_post(); ?>
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="akce_item">
						<div class="archiv_ref_img" style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'medium')?>) center / cover no-repeat;"></div>
                        <h4><?= get_the_title() ?></h4>
                        <div class="akce_date"><?= get_the_date() ?></div>
                        <p><?= wp_trim_words(get_the_content(), 35, '...') ?></p>
                        <a href="<?= get_permalink(); ?>" class="akce_item_btn">Zobrazit referenci</a>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="col-md-12">
            <div class="row blog_pagi">

                <?php the_posts_pagination( array(
                    'prev_text'          => __( '<i class="icon icon-arrow-left"></i><span class="pagi_prev">Předchozí</span>', 'va' ),
                    'next_text'          => __( '<span class="pagi_next">Další</span><i class="icon icon-arrow-right"></i>', 'va' ),
                    'screen_reader_text'          => ' ',
                ) ); ?>


            </div>
        </div>
    </div>
</div>
