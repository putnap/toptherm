<?php get_template_part('templates/page', 'header'); ?>
<div class="archiv akce_holder">
    <div class="container">
        <div class="row akce_item_holder">
            <?php while (have_posts()) :
                the_post(); ?>
                <div class="col-md-6 col-12">
					<div class="akce_item row">
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-4 col-5">
							<a class="akce_bg" href="<?= get_permalink(); ?>" style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'rp_pdf'); ?>) left top no-repeat;"></a>
						</div>
						<div class="col-xl-8 col-lg-8 col-md-12 col-sm-8 col-7">
							<h4><?= get_the_title() ?></h4>
							<div class="akce_date"><?= get_the_date() ?></div>
							<p><?= wp_trim_words(get_the_content(), 55, '...') ?></p>
							<a href="<?= get_permalink(); ?>" class="akce_item_btn">ČÍST CELÉ</a>
						</div>
					</div>
                </div>
            <?php endwhile; ?>
        </div>
        <div class="col-md-12">
            <div class="row blog_pagi">
                <?php the_posts_pagination( array(
                    'prev_text'          => __( '<i class="icon icon-arrow-left"></i><span class="pagi_prev">Předchozí</span>', 'va' ),
                    'next_text'          => __( '<span class="pagi_next">Další</span><i class="icon icon-arrow-right"></i>', 'va' ),
                    'screen_reader_text'          => ' ',
                ) ); ?>
            </div>
        </div>
    </div>
</div>
