<?php
use Roots\Sage\Extras;
use Roots\Sage\Titles;
?>
<div class="page_header">
	<div class="container">
		<div class="row">
			<div class="col-12 breadcrumb_holder">
				<?php Extras\custom_breadcrumbs(); ?>
			</div>
			<div class="col-12">
				<h2><b><?= Titles\title() ?></b></h2>
			</div>
			
			<!--<div class="col-md-8 col-12">
				<h2><b><?/*= Titles\title() */?></b></h2>
			</div>
			<div class="col-md-4 col-12 breadcrumb_holder">
                <?php /*Extras\custom_breadcrumbs(); */?>
			</div>-->
		</div>
	</div>
</div>
