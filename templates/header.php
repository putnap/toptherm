<header class="banner">
	<div class="top_header clearfix">
		<div class="container">
			<div class="top_header_menu">
				<div class="menu_holder ml-auto">
					<?php wp_nav_menu( array(
						'theme_location' => 'top_navigation',
						'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
					) ); ?>
				</div>
			</div>
			<div class="top_header_social">
				<a href="#">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="#">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-toggleable-lg navbar-inverse">
	  <div class="container">
		  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
		  </button>
		  <a class="navbar-brand" href="<?= get_home_url() ?>">
			  <img src="<?= get_template_directory_uri().'/dist/images/logo.png' ?>" alt="Top therm">
		  </a>

		  
		  <div id="navbar" class="navbar-collapse collapse">
			  <div class="menu_holder ml-auto">
				  <?php wp_nav_menu( array(
					  'theme_location' => 'primary_navigation',
					  'items_wrap'     => '<ul class="%2$s nav navbar-nav">%3$s</ul>',
				  ) ); ?>
			  </div>
		  </div>
	  </div>
	</nav>

</header>
