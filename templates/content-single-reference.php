<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class(); ?>>
        <header>
            <?php get_template_part('templates/entry-meta'); ?>
        </header>
        <div class="entry-content">
            <?php the_content(); ?>
            <div class="gal_hlder">
            <?php
            $gal = get_field('reference_gal');
            foreach ($gal as $img){ ?>
                <a href="<?= $img['url'] ?>">
                    <img src="<?= $img['sizes']['medium'] ?>" alt="">
                </a>
            <?php }
            ?>
            </div>
        </div>
    </article>
<?php endwhile; ?>
