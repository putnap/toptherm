<footer class="content-info footer">
  <div class="container">
    <div class="row">
		<div class="col-lg-3 col-sm-6 col-12">
            <?php dynamic_sidebar('sidebar-footer1'); ?>
        </div>
		<div class="col-lg-3 col-sm-6 col-12 fakturacni_udaje">
            <?php dynamic_sidebar('sidebar-footer2'); ?>
		</div>
		<div class="col-lg-3 col-sm-6 col-12 kontakt_footer">
            <?php dynamic_sidebar('sidebar-footer3'); ?>
		</div>
		<div class="col-lg-3 col-sm-6 col-12">
            <?php dynamic_sidebar('sidebar-footer4'); ?>
		</div>
	</div>
  </div>
	<div class="container subfooter">
		<div class="row">
			<div class="col-12">
				<p>Copyright © 2017 KTO international s.r.o. All Rights Reserved.</p>
				<p class="small-text">Created by <a href="//storypress.cz/" target="_blank">StoryPress</a></p>
			</div>
		</div>
	</div>
</footer>
