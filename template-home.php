<?php
/**
 * Template Name: Home
 */

$args = array(
    'post_type' => 'slider',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1,
);
$posty = new \WP_Query($args);

?>


<div class="home_slider_holder">
    <div id="home_slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            for ($i = 0; $i < $posty->post_count; $i++) { ?>
                <li data-target="#home_slider" data-slide-to="<?= $i ?>" <?= $i == 0 ? 'class="active"' : '' ?>></li>
            <?php }
            ?>
        </ol>
        <div class="carousel-inner" role="listbox">
            <?php
            $index = 0;
            while ($posty->have_posts()) : $posty->the_post(); ?>
                <div class="carousel-item <?= $index == 0 ? 'active' : '' ?>"
                     style="background: url(<?= get_the_post_thumbnail_url(get_the_ID(), 'full') ?>) center no-repeat;">
                    <div class="slider_overlay"></div>
                    <div class="carousel-caption text-center">
                        <h2><?= get_the_title(); ?></h2>
                        <p><?= get_the_content(); ?></p>
                    </div>
                </div>
                <?php
                $index++;
            endwhile;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>

<?php while (have_posts()) : the_post(); ?>

    <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
