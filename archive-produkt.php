<?php

$terms = get_terms(array(
    'taxonomy' => 'produkt_cat',
    'hide_empty' => false,
));

get_template_part('templates/page', 'header'); ?>
<div class="page_katalog_holder section_pad_70">
	<div class="container">
		<div class="row">
            <div class="col-lg-3 col-md-4 col-12">
                <ul class="katalog_nav">
                    <?php
                    foreach ($terms as $term) { ?>
                        <li><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
                    <?php } ?>
                </ul>
            </div>
			<div class="col-lg-9 col-md-8 col-12">
                <?php

                $index = 1;
                foreach ($terms as $term){
                    $katalog_item = get_field('');
                    ?>
                    <div class="kategory_item_holder">
                        <div class="kat_number"><i class="fa fa-gears" aria-hidden="true"></i></div>
                        <div class="kat_content">
							<h3><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></h3>
                            <p><?= get_field('popis_katalog',$term); ?></p>
							<div class="gal_hlder">
                                <?php
                                foreach ( get_field('galerie',$term) as $img){ ?>
                                    <a href="<?= $img['url']?>">
                                        <img src="<?= $img['sizes']['medium']?>" alt="<?= $katalog_item['nazev'] ?>">
                                    </a>
                                <?php }
                                ?>
							</div>
                        </div>
                    </div>
                <?php
                $index++;
                }
                ?>
			</div>
		</div>
	</div>
</div>
